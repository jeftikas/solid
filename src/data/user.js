export const userData = {
    firstName: "Pepe",
    lastName: "Argento",
    birthDate: new Date('1982-08-26'),
    gender: "Masculino",
    email: "email@email.com",
    role: "Admin",
    lastAccess: new Date('2023-09-08 12:15:56'),
    workAreas: ["/usr/home", "/home"]
  };
  